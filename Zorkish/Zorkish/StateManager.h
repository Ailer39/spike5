#pragma once

#include "StateBase.h"

class StateManager
{
private:
	int _currentStateIndex;
public:
	StateManager();
	~StateManager() {};
	void ChangeState(int state);
	void ChangeToGameState(int level);
};