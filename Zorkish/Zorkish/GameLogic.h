#pragma once
#include "StateBase.h"
class GameLogic :
	public StateBase
{
private:
	GameLogic() {};
public:
	static GameLogic* GetInstance();
	void Show(StateManager* context, int level);
	~GameLogic();
};