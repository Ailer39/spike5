#pragma once
#include "StateBase.h"

class SelectAdventure :
	public StateBase
{
private:
	SelectAdventure() {};
public:
	static SelectAdventure* GetInstance();
	void Show(StateManager* context);
};