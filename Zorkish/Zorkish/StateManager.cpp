#include "StateManager.h"
#include "MainState.h"
#include "About.h"
#include "Help.h"
#include "SelectAdventure.h"
#include "GameLogic.h"
#include "Highscore.h"

StateBase* states[8];

StateManager::StateManager()
{
	_currentStateIndex = 0;
	states[0] = MainState::GetInstance();
	states[1] = SelectAdventure::GetInstance();
	states[2] = Highscore::GetInstance();
	states[3] = Help::GetInstance();
	states[4] = About::GetInstance();
	states[6] = GameLogic::GetInstance();
	this->ChangeState(0);
}

void StateManager::ChangeToGameState(int level)
{
	_currentStateIndex = 6;
	((GameLogic*)states[_currentStateIndex])->Show(this, level);
}

void StateManager::ChangeState(int state)
{
	if (state == 5)
	{
		exit(0);
	}
	else if (state < 5)
	{
		_currentStateIndex = state;
		states[_currentStateIndex]->Show(this);
	}
}