#include "GameLogic.h"

static GameLogic* instance;

GameLogic* GameLogic::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new GameLogic();
	}

	return instance;
}

void GameLogic::Show(StateManager* context, int level)
{
	cout << "Level: " << level << endl;
	cout << "You have entered the magic word and will now see the New High Score screen." << endl << endl;
	context->ChangeState(2);
}