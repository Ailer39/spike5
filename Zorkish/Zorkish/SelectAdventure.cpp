#include "SelectAdventure.h"


static SelectAdventure* instance;

SelectAdventure* SelectAdventure::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new SelectAdventure();
	}

	return instance;
}

void SelectAdventure::Show(StateManager* context)
{
	cout << "Zorkish::Select Adventure" << endl << endl;
	cout << "Choose your adventure:" << endl << endl;
	cout << "1. Mountain World" << endl;
	cout << "2. Water World" << endl;
	cout << "3. Box World" << endl;
	cout << "Select 1-3: _";
	int level;
	cin >> level;

	context->ChangeToGameState(level);
}